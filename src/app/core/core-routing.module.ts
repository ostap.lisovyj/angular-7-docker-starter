import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PATH } from '@app/shared/constants';

const routes: Routes = [
  {
    path: PATH.ROOT,
    redirectTo: PATH.DASHBOARD,
    pathMatch: 'full'
  },
  {
    path: PATH.DASHBOARD,
    loadChildren: '../dashboard/dashboard.module#DashboardModule',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
