# Angular 7 starter app using Core-Shared module structure + Docker

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.1.


## Requirements
Installed [Docker](https://www.docker.com/products/docker-desktop) (latest 18.x.x)

## Module Structure
Current Angular Starter project follows the Core-Shared module structure. `Core module` is ensured to be instantiated only once and so is a great place to hold singleton services which can be shared among other feature modules. `Shared module` is a place to keep all 'dumb' components (check-boxes, menus, widgets, etc). See the following instructions for more details - https://www.technouz.com/4772/angular-6-app-structure-with-multiple-modules/


## Dev Environment

- Build the docker image by running the following command: `docker build -t ng-app-chromium .`

- Install all yarn dependencies by running the following:
```
docker run -t --rm -v `pwd`:/usr/app -w /usr/app ng-app-chromium yarn
```
- Add npm/yarn dependencies only within docker container:
```
docker run -t --rm -v `pwd`:/usr/app -w /usr/app ng-app-chromium yarn add lodash
```

## Development server

- Run `docker-compose up`
- Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run the following to generate a new component:
```
docker run -t --rm -v `pwd`:/usr/app -w /usr/app ng-app-chromium ng generate component component-name
```
You can also use `... ng generate directive|pipe|service|class|guard|interface|enum|module`.


## Build

Run the following to build the project (The build artifacts will be stored in the `dist/` directory):
```
docker run -t --rm -v `pwd`:/usr/app -w /usr/app ng-app-chromium yarn build
```


## Running unit tests

Run the following to execute the unit tests via [Karma](https://karma-runner.github.io):
```
docker run -t --rm -v `pwd`:/usr/app -w /usr/app ng-app-chromium yarn test
```


## Running end-to-end tests

Run the following to execute the end-to-end tests via [Protractor](http://www.protractortest.org/):
```
docker run -t --rm -v `pwd`:/usr/app -w /usr/app ng-app-chromium yarn e2e
```
